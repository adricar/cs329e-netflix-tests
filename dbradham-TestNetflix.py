#!/usr/bin/env python3

# -------
# imports
# -------
import Netflix
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    def test_customer_avg_1(self):
        customer = 2195487
        year = 2004
        self.assertEqual(Netflix.customer_avg(customer, year), 3.309)
    def test_customer_avg_2(self):
        customer = 897070
        year = 2005
        self.assertEqual(Netflix.customer_avg(customer, year), 3.244)
    def test_customer_avg_3(self):
        customer = 941840
        year = 2003
        self.assertEqual(Netflix.customer_avg(customer, year), 3.452)


    def test_year_avg_1(self):
        movie = 9212
        year = 2001
        self.assertEqual(Netflix.year_avg(movie,year),3.667)

    def test_year_avg_2(self):
        movie = 9598
        year = 2005
        self.assertEqual(Netflix.year_avg(movie,year),3.854)

    def test_year_avg_3(self):
        movie = 3393
        year = 2002
        self.assertEqual(Netflix.year_avg(movie,year), 2.0)


    # -------------
    # calculate_avg
    # -------------

    def test_calculate_avg_1(self):
        customer = 882174
        movie = 1561
        self.assertEqual(Netflix.calculate_avg(movie, customer), 3.7575)
    def test_calculate_avg_2(self):
        customer = 1967331
        movie = 3138
        self.assertEqual(Netflix.calculate_avg(movie, customer), 3.752)
    def test_calculate_avg_3(self):
        customer = 1998541
        movie = 3371
        self.assertEqual(Netflix.calculate_avg(movie, customer), 3.5555)

    def test_actual_score_1(self):
        customer = 2243990
        movie = 10947
        self.assertEqual(Netflix.actual_score(customer, movie), 5)

    def test_actual_score_2(self):
        customer = 1543996
        movie = 3514
        self.assertEqual(Netflix.actual_score(customer, movie), 2)

    def test_actual_score_3(self):
        customer = 291587
        movie = 12526
        self.assertEqual(Netflix.actual_score(customer, movie), 3)

    # ----
    # eval
    # ----

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
