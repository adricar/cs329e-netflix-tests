#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----
    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.0\n2.8\n3.5\n0.78\n")
    
    def test_eval_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10:\n2.9\n2.7\n0.18\n")

    def test_eval_3(self):
        r = StringIO("10038:\n885147\n1144499\n1411630\n1069925\n836219\n1417121\n461985\n1148007\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10038:\n4.0\n3.2\n3.0\n3.2\n3.0\n3.5\n3.5\n2.8\n0.61\n")

    def test_eval_4(self):
        r = StringIO("915:\n2261623\n1473980\n2184520\n1321416\n1051251\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "915:\n2.6\n2.3\n3.3\n2.4\n2.9\n1.08\n")

    def test_rsme_test(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r,w)
        w_list = w.getvalue().strip().split(sep="\n")
        rsme_to_check = float(w_list[-1])

        self.assertLess(
            rsme_to_check, 1
        )
    def test_movie_id(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        r_list = r.getvalue().strip().split(sep="\n")
        for number in r_list:
            if number[-1] == ":":
             movie_id_check = int(number[:-1])
             self.assertLessEqual(movie_id_check, 17700)
             self.assertGreater(movie_id_check, 0)
      
    
    def test_customer_id(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        r_list = r.getvalue().strip().split(sep="\n")
        for i in r_list:
            if i[-1] != ":":
                check_value = int(i)
                self.assertLessEqual(check_value, 2649429)
                self.assertGreater(check_value, 0)

    def test_movie_ratings(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        w_list = w.getvalue().strip().split(sep="\n")
        for number in w_list[:-1]:
            if number[-1] != ":": #exclude movie id
                check_value = float(number)
                self.assertGreaterEqual(check_value, 1.0)
                self.assertLessEqual(check_value, 5.0)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
